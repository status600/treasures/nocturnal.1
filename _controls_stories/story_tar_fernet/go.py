

'''
	python3 status.proc.py "activities/tar/_status/drive_dir_to_memory_tar/status_1.py"
'''


import shutil
from os.path import dirname, join, normpath
import pathlib
import sys
import zipfile

def add_paths_to_system (paths):
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'/habitat/parties'
])

#/
#
from cryptography.fernet import Fernet
#
from nocturnal.activities.fernet_1.memory_to_memory_encrypted import memory_to_memory_encrypted
from nocturnal.activities.fernet_1.key_scan import scan_fernet_1_key
from nocturnal.activities.fernet_1.key_produce import produce_fernet_1_key
#
from ships.paths.directory.tar.drive_directory_to_memory_tar import drive_directory_to_memory_tar	
from ships.paths.directory.tar.memory_tar_to_drive_directory import memory_tar_to_drive_directory	

#
from nocturnal.activities.FS.file.delete_abandon import delete_abandon_file	
from nocturnal.activities.FS.directory.delete_abandon import delete_abandon_directory
#
from nocturnal.activities.FS.file.etch import etch_file
from nocturnal.activities.FS.file.scan import scan_file
#
#
import ships.paths.directory.check_equality as check_equality
#
#\
from nocturnal.activities.fernet_1_tar.drive_directory_to_drive_fernet import drive_directory_to_drive_fernet
	

	

this_folder = pathlib.Path (__file__).parent.resolve ()
original_directory_path = str (normpath (join (this_folder, "constants/directory_1")))

tar_path_without_extension = str (normpath (join (this_folder, "variance/directory_1")))
tar_path = str (normpath (join (this_folder, "variance/directory_1.tar")))

reversed_directory_path = str (normpath (join (this_folder, "variance/directory_1")))

fernet_key_path = str (normpath (join (this_folder, "variance/fernet.key.JSON")))

tar_fernet_path = str (normpath (join (this_folder, "variance/directory_1.tar.fernet")))
tar_fernet_decrypt_path = str (normpath (join (this_folder, "variance/directory_1.tar.fernet.decrypted")))


def abandon_file_attempt (file_path):
	try:
		delete_abandon_file (file_path)
	except Exception:
		pass;

def abandon_directory_attempt (directory_path):
	try:
		delete_abandon_directory (directory_path)
	except Exception:
		pass;

abandon_file_attempt (tar_path)
abandon_directory_attempt (reversed_directory_path)

#
#
#
#

produce_fernet_1_key ({
	"write_outputs": "yes",
	"outputs": {
		"fernet_key_path": fernet_key_path
	}
});


#/
#
#	encrypt
#
def encrypt ():
	'''
	tar_stream = drive_directory_to_memory_tar ({
		"directory_path": original_directory_path
	})
	memory_tar_to_drive_directory ({
		"tar_stream": tar_stream,
		"directory_path": reversed_directory_path
	})
	memory_encrypted = memory_to_memory_encrypted ({
		"fernet_1_key": scan_fernet_1_key (fernet_key_path),
		"bytes_io": tar_stream
	})
	etch_file ({
		"path": tar_fernet_path,
		"strand": memory_encrypted
	})
	'''
	
	drive_directory_to_drive_fernet ({
		"fernet_1_key": scan_fernet_1_key (fernet_key_path),
		
		"directory_path": original_directory_path,
		"tar_fernet_file_path": tar_fernet_path
	})
	
encrypt ()
#
#\

#/
#
#	decrypt
#
from nocturnal.activities.fernet_1.memory_encrypted_to_memory import memory_encrypted_to_memory
from nocturnal.activities.fernet_1.key_scan import scan_fernet_1_key
from nocturnal.activities.fernet_1_tar.drive_fernet_to_drive_directory import drive_fernet_to_drive_directory

import io

def decrypt ():
	'''
	fernet_1_key = scan_fernet_1_key (fernet_key_path);
	
	strand = scan_file ({
		"path": tar_fernet_path
	})
	
	decrypted = memory_encrypted_to_memory ({
		"fernet_1_key": fernet_1_key,
		"strand": strand
	})
	
	bytes_io = io.BytesIO (b"")
	bytes_io.write (decrypted)
	bytes_io.seek(0)
	
	tar_stream = bytes_io
	
	memory_tar_to_drive_directory ({
		"tar_stream": tar_stream,
		"directory_path": tar_fernet_decrypt_path
	})
	'''
	
	
	
	drive_fernet_to_drive_directory ({
		"fernet_1_key": scan_fernet_1_key (fernet_key_path),
		
		"tar_fernet_path": tar_fernet_path,
		"directory_path": tar_fernet_decrypt_path
	})
	
	report = check_equality.start (
		original_directory_path,
		tar_fernet_decrypt_path
	)	
	assert (
		report ==
		{'1': {}, '2': {}}
	), report
	
decrypt ()

