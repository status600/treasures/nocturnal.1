
deactivate


#
#	source /habitat/_controls/source_1.sh
#
git config --global --add safe.directory /habitat

#/
#
#	non-custom habitat install
#
#		https://github.com/astral-sh/uv
#
apt install unzip; curl -fsSL https://bun.sh/install | bash; . /root/.bashrc

cd /habitat && rm -rf .venv
pip install uv poetry
uv cache clean
cd /habitat && rm requirements.txt
cd /habitat && uv pip compile pyproject.toml -o requirements.txt

sleep 2

cd /habitat && uv venv
cd /habitat && . /habitat/.venv/bin/activate
cd /habitat && uv pip sync requirements.txt
#
#\


# deactivate



#
#
#
#
. /habitat/_controls/build_2.sh

#
#
#
#