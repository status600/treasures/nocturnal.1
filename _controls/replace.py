



def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_directory = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_directory, path)))

add_paths_to_system ([
	'/habitat/parties'
])


import ships.paths.directory.find_and_replace_string_v2 as find_and_replace_string_v2
import pathlib
from os.path import dirname, join, normpath


places = [
	"/habitat",
	"/habitat/parties"
]

for place in places:
	print ("place:", place)

	find_and_replace_string_v2.start (
		the_path = place,

		find = 'nocturnal',
		replace_with = 'nocturnal',
		
		replace_contents = "yes",
		replace_paths = "yes"
	)